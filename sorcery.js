$( function() {

	var life = 0;
	var kit = 0;
	var desk = 0;
	var saves = 0;
	var buttons = $(".section button");
	var status = $("#status");
	var buttons = $(".section button");
	var nom;
	var audio ;
	var ambiance ;
	var ambiancerapide;
	var typing = new Audio('sound/typing.mp3');
	var x;
	var p;
	var help;
	var mdp=0;
	var cookieKey;
	var codeur;
	startGame();
	// $('#saveButton').prop('disabled', true);
	// $('#saveButton').prop('disabled', false);
	// var erased_cookieKey = $.jCookies({ erase : 'Key' });  enlever le commentaire si problème de sauvegarde
	cookieKey = $.jCookies({ get : 'Key' });
	console.log(cookieKey);

	// if (cookieKey !=""){
	// 	gotoSection(cookieKey);
	// 	$("#intro").hide();
	// }
	$("#saveButton").click( function(){
		// console.log("test");
		// var erased_cookieKey = $.jCookies({ erase : 'Key' });
		// console.log(erased_cookieKey);
		// $.jCookies({
		// 	name : 'Key',
		// 	value : $(this).attr('class')
		// });
		$("#saveText").slideDown();
	})


	buttons.click( function() {
		//...
		$(this).closest("div").hide();
		var key = $(this).attr("go");
		gotoSection(key);

		var illustration = $(this).attr("illustration");
		if(illustration)
			gotoSection(illustration);

		if($(this).hasClass("lifelost")){
			loseOneLife();
		}

		if($(this).is("#fail1")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail1").hide();
		}
		if($(this).is("#fail2")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail2").hide();
		}
		if($(this).is("#fail3")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail3").hide();	
		}
		if($(this).is("#fail4")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail4").hide();
		}
		if($(this).is("#fail5")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail5").hide();
		}
		if($(this).is("#fail6")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail6").hide();
		}
		if($(this).is("#fail7")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail7").hide();
		}
		if($(this).is("#fail8")){
			$("#help ").remove();
			$("#question").remove();
			help='<button go="fail" class="skilllost" id="help">* Utiliser son skill pour trouver un indice *</button>';
			$("#fail ").append(help);
			$("button").fadeIn(1500);
			$("#fail #fail8").hide();
		}

		if($(this).is("#btn_location")){
			$("#btn_true ").remove();
		}
		if($(this).is("#btn_aide")){
			$("#btn_trueComputer ").remove();
			$("#btn_location ").remove();
		}

		if($(this).is("#btn_aideLocation")){
			$("#btn_locationComputer ").remove();
		}

		if($(this).hasClass("skilllost"))
			loseOneSkills();

		if($(this).is("#help2")){
			$("#help2").remove();
			var indice='<p id="indice"> Et si tu essayais en tapant cactus ? </p>';
			$("#computer ").append(indice);
		}

		if(key=='aide'){
 			manMove();
		}

		if(key=='fail'){
 			$(document).on('click','#help', function()
			{
			    $("#help ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<a href="https://translate.google.fr/" target="Translate">je crois que cela pourra etre utile</a>'
				$("#fail ").append(indice);
			});
		}

		if(key=='defi1'){
 			$(document).on('click','#helpdefi1', function()
			{ if(kit!=0){
			    $("#helpdefi1 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice"><a href="https://fr.wikipedia.org/wiki/Concaténation" target="Concaténation">Concaténation</a>,  <a href="https://fr.wikipedia.org/wiki/Nombre_premier" target="Nombre_premier">Nombre premier </a> et <a href="https://fr.wikipedia.org/wiki/Système_binaire" target="Système_binaire">Binaire</a> vont parfois de paire </p>'
				$("#defi1 ").append(indice);	
				}else{
				$("#helpdefi1 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice">Plus de skills disponibles</p>'
				$("#defi1 ").append(indice);	
				}
			});
		}

		if((key=='defi')&&(codeur==1)){
			var last='<button go="defilast" id="lastbutton">* Sauver Jérome *</button>';
			$("#defi ").append(last);
		}

		if(key=='defi'){
 			$(document).on('click','#lastbutton', function()
			{ 
				gotoSection("defilast")
				$("#defi ").hide();
			});
		}

		if(key=='defi2'){
 			$(document).on('click','#helpdefi2', function()
			{ if(kit!=0){
			    $("#helpdefi2 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice">Un histoire de <a href="http://www.unit-conversion.info/texttools/hexadecimal/" target="Hexa"> Conversion </a></p>'
				$("#defi2 ").append(indice);	
				}else{
				$("#helpdefi2 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice">Plus de skills disponibles</p>'
				$("#defi2 ").append(indice);	
				}
			});
		}

		if(key=='defi3'){
 			$(document).on('click','#helpdefi3', function()
			{ if(kit!=0){
			    $("#helpdefi3 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice">Un peu de <a href="https://copy.sh/brainfuck/" target="Brainfuck"> Brainfuck </a></p>'
				$("#defi3 ").append(indice);	
				}else{
				$("#helpdefi3 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice">Plus de skills disponibles</p>'
				$("#defi3 ").append(indice);	
				}
			});
		}

		if(key=='defi4'){
 			$(document).on('click','#helpdefi4', function()
			{ if(kit!=0){
			    $("#helpdefi4 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice"> Comment lire le <a href="http://www.lexilogos.com/clavier/braille_conversion.htm" target="Braille"> Braille </a></p>'
				$("#defi4 ").append(indice);	
				}else{
				$("#helpdefi4 ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice">Plus de skills disponibles</p>'
				$("#defi4 ").append(indice);	
				}
			});
		}

		if(key=='defilast'){
 			$(document).on('click','#helpdefilast', function()
			{ if(kit!=0){
			    $("#helpdefilast ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice"> Un dernier <a href="http://morsecode.scphillips.com/translator.html" target="Morse"> Indice </a></p>'
				$("#defilast ").append(indice);	
				}else{
				$("#helpdefilast ").remove();
				$("button").fadeIn(1500);
				loseOneSkills();
				var indice='<p id="indice">Plus de skills disponibles</p>'
				$("#defilast ").append(indice);	
				}
			});
		}
		if(key=='intro'){
			location.reload();
 			restart();
		}

		if(key=='computer'){
 			mdp=1;
		}

		// if(key=='joinOrDie'){
		// 	$('body').css('background-image', 'url("img/cactus.jpg")');
		// 	$('body').css('background-size', 'cover');
		// }



		if(key=='answer'){
			ambiance = new Audio('sound/ambiance.mp3');
			ambiance.loop=true;
	  		ambiance.play();
		}

	} );

	$("#personne").click( function(event){
		event.preventDefault();
		nom = $("#prenom").val();
		if (nom.length==0){
			var no="<p>Champ obligatoire ! Je veux savoir qui tu es.</p>";
			$("#answer .typed-strings").append(no);
			$("#question").remove();
			gotoSection('answer');
			audio = new Audio('sound/obligatoire.wav');
  			audio.play();
		}
		else{
			var p = "<p>Bonjour " + nom  +" ! Tu peux m'aider ? Je suis coincé ici et tu es la seule personne que j'ai réussi à contacter ...</p>";
			$("#reponse .typed-strings").prepend(p);
			gotoSection('reponse');
			$("#answer").hide();
			p = "<p >Ressaisis toi " + nom  +" ! Je suis en danger. Aide moi ! </p>";
			$("#refus .typed-strings").prepend(p);
		}
	})

	$("#reponsedefi1").click( function(event){
		event.preventDefault();
		var reponse1 = $("#reponse1").val();
		if (reponse1!=100101){
			var no="<p>Mauvaise réponse ! Essaye encore de trouver la suite de cette suite : <br> 1011101111101111011000110011101111110111111</p>";
			$("#defi1 .typed-strings").append(no);
			$("#defi1contenu").remove();
			help='<button go="defi1" class="skilllost" id="helpdefi1">* Utiliser son skill pour trouver un indice *</button>';
			$("#defi1 ").append(help);
			gotoSection('defi1');
			loseOneLife();
		}
		else{
			codeur--;
			$("#defi1").hide();
			p = "<p >Bien joué " + nom  +" ! Encore "+ codeur+" codeurs à sauver. </p>";
			$("#defiwin .typed-strings").prepend(p);
			gotoSection('defiwin');
			$("#defi1button").remove();
			$('#coder1').attr('src','img/coder1saved.jpg');
		}
	})

	$("#reponsedefi2").click( function(event){
		event.preventDefault();
		var reponse2 = $("#reponse2").val();
		if (reponse2!=52){
			var no="<p>Mauvaise réponse ! Essaye encore de trouver la réponse de cette question : <br> 43 6f 64 65 20 68 65 78 61 20 64 75 20 72 20 6d 61 6a 75 73 63 75 6c 65 20 3f</p>";
			$("#defi2 .typed-strings").append(no);
			$("#defi2contenu").remove();
			help='<button go="defi2" class="skilllost" id="helpdefi2">* Utiliser son skill pour trouver un indice *</button>';
			$("#defi2 ").append(help);
			gotoSection('defi2');
			loseOneLife();
		}
		else{
			codeur--;
			$("#defi2").hide();
			p = "<p >Bien joué " + nom  +" ! Encore "+ codeur+" codeurs à sauver. </p>";
			$("#defiwin .typed-strings").prepend(p);
			gotoSection('defiwin');
			$("#defi2button").remove();
			$('#coder2').attr('src','img/coder2saved.jpg');
		}
	})

	$("#travail").click( function(event){
		setTimeout(function(){
		  location.reload();
		}, 2000);
		
	})

	$("#reponsedefi3").click( function(event){
		event.preventDefault();
		var reponse3 = $("#reponse3").val();
		if (reponse3!="Bordeaux"){
			var no="<p>Mauvaise réponse ! Essaye encore de trouver le mot de passe : <br> ++++[++++>---<]>-.-[--->+<]>++++.+++.--------------.+.----.--[--->+<]>.+++.</p>";
			$("#defi3 .typed-strings").append(no);
			$("#defi3contenu").remove();
			help='<button go="defi3" class="skilllost" id="helpdefi3">* Utiliser son skill pour trouver un indice *</button>';
			$("#defi3 ").append(help);
			gotoSection('defi3');
			loseOneLife();
		}
		else{
			codeur--;
			$("#defi3").hide();
			p = "<p >Bien joué " + nom  +" ! Encore "+ codeur+" codeurs à sauver. </p>";
			$("#defiwin .typed-strings").prepend(p);
			gotoSection('defiwin');
			$("#defi3button").remove();
			$('#coder3').attr('src','img/coder3saved.jpg');
		}
	})

	$("#reponsedefi4").click( function(event){
		event.preventDefault();
		var reponse4 = $("#reponse4").val();
		if (reponse4!="Université"){
			var no="<p>Mauvaise réponse ! Essaye encore de trouver le mot de passe : <br> ⠨⠥⠝⠊⠧⠑⠗⠎⠊⠞⠿</p>";
			$("#defi4 .typed-strings").append(no);
			$("#defi4contenu").remove();
			help='<button go="defi4" class="skilllost" id="helpdefi4">* Utiliser son skill pour trouver un indice *</button>';
			$("#defi4 ").append(help);
			gotoSection('defi4');
			loseOneLife();
		}
		else{
			codeur--;
			$("#defi4").hide();
			p = "<p >Bien joué " + nom  +" ! Encore "+ codeur+" codeurs à sauver. </p>";
			$("#defiwin .typed-strings").prepend(p);
			gotoSection('defiwin');
			$("#defi4button").remove();
			$('#coder4').attr('src','img/coder4saved.jpg');
		}
	})

	$("#reponsedefilast").click( function(event){
		event.preventDefault();
		var reponselast = $("#reponselast").val();
		if (reponselast!="FIN"){
			var no="<p>Mauvaise réponse ! Essaye encore de trouver le mot de passe : <br> ..-. .. -.</p>";
			$("#defilast .typed-strings").append(no);
			$("#defilastcontenu").remove();
			help='<button go="defilast" class="skilllost" id="helpdefilast">* Utiliser son skill pour trouver un indice *</button>';
			$("#defilast ").append(help);
			gotoSection('defilast');
			loseOneLife();
		}
		else{
			codeur--;
			$("#defilast").hide();
			p = "<p >Merci " + nom  +" ! Tu as réussi à tous nous sauver.<br>Nous t'en serons éternellement reconnaissant. </p>";
			$("#fin .typed-strings").prepend(p);
			ambiancerapide.pause();
			ambiance = new Audio('sound/win.mp3');
			ambiance.loop=true;
	  		ambiance.play();
	  		$('body').css('background-image', 'url("img/ENDGAME.jpg")');
			$('body').css('background-size', 'cover');
			gotoSection('fin');
		}
	})
	

		$("#coffee").click( function(){
			if(desk!=0){
				$(".desklost").hide();	
				loseOneDesk();
				}else{
				$(".desklost").hide();
				var indice='<p id="indice">Plus de café disponibles</p>'
				$("#defi").append(indice);	
				}
		})
	
	function gotoSection(key) {
		//...
		console.log(key);
		ecriture(key);
		$("#man").hide();
		$("#saveText").hide();
		$("button").hide();
		nom = $("#prenom").val();
		$("[id='" + key + "']").fadeIn(1000);
		if(key!='fail'){
			$("button").fadeIn(1000);
		}
		audio.pause();
		if(key=='defi1'){
			$('#defi1contenu').fadeIn(1000);
		}
		if(key !='death'){	
			typing.pause();		
			typing = new Audio('sound/typing.mp3');
	  		typing.play();
		}else{
			typing.pause();
			ambiance.pause();
			ambiancerapide.pause();
			audio = new Audio('sound/death.mp3');
	  		audio.play();
			$('body').css('background-image', 'url("img/backDeath.gif")');
			$('body').css('background-size', 'cover');
			$('#death').css('background-color', 'black');
			$('#death').css('padding-top', '20px');
			}
			$("body").removeClass("class2");
			$("body").removeClass("class3");
			clearInterval(x);
			$("#saveButton").removeClass();
			$("#saveButton").addClass(key);
	}
	/**********Cactus*/
// a key map of allowed keys
var allowedKeys = {
  65: 'a',
  67: 'c',
  83: 's',
  84: 't',
  85: 'u'
};

// the 'official' Konami Code sequence
var konamiCode = ['c', 'a', 'c', 't', 'u', 's'];

// a variable to remember the 'position' the user has reached so far.
var konamiCodePosition = 0;

// add keydown event listener
document.addEventListener('keydown', function(e) {
  // get the value of the key code from the key map
  var key = allowedKeys[e.keyCode];
  // get the value of the required key from the konami code
  var requiredKey = konamiCode[konamiCodePosition];

  // compare the key with the required key
  if (key == requiredKey) {

    // move to the next key in the konami code sequence
    konamiCodePosition++;

    // if the last key is reached, activate cheats
    if (konamiCodePosition == konamiCode.length)
      activateCheats();
  } else
    konamiCodePosition = 0;
});





function activateCheats() {
	if(mdp==1){
		gotoSection('cactus');
		$("#computer").hide();
		mdp=0;
		audio = new Audio('sound/granted.wav');
  		audio.play();
  		$('body').css('background-image', 'url("img/hack.gif")');
		$('body').css('background-size', 'cover');
		$('#cactus').css('background-color', 'black');
		ambiance.pause();
		ambiancerapide = new Audio('sound/ambiance-rapide.mp3');
		ambiancerapide.loop=true;
  		ambiancerapide.play();
  		$(".coder ").hide();

		 		//Blink settings
		var blink = {
		    obj: $(".coder"),
		    timeout: 8000,
		    speed: 1000
		};

		//Start function
		blink.fn = setInterval(function () {
		    blink.obj.fadeToggle(blink.speed);
		}, blink.speed + 1);

		//Ends blinking, after 'blink.timeout' millisecons
		setTimeout(function () {
		    clearInterval(blink.fn);
		    //Ensure that the element is always visible
		    blink.obj.fadeIn(blink.speed);
		    blink = null;
		}, blink.timeout);
	}
}

//*******Life
	function getOneLife() {
		//...
		if(life<3){
			$("img.life").eq(life).show();
			life++;
		}
	}
	
	function setLife(v) {
		//...
		life = v;
	}
	
	function loseOneLife() {
		//...
		$("img.life").eq(life-1).hide();
		x= setInterval(function(){
  			$("body").toggleClass("class2");
  			$("body").toggleClass("class3");
  			$("body").toggleClass("class2");

  		},500)
		life--;
		audio = new Audio('sound/electric.wav');
  		audio.play();
		$('#loose').css('margin-left', '25%');
		$('#loose').slideDown(500);
		$('#loose').slideUp(300);
		if(life==0){
			endGame();
		}
		
	}

//*******Skills
	function getOneSkills() {
		//...
		if(kit<3){
			$("img.kit").eq(kit).show();
			kit++;
		}
	}
	
	function setSkills(v) {
		//...
		kit = v;
	}
	
	function loseOneSkills() {
		//...
		$("img.kit").eq(kit-1).hide();
		kit--;
		audio = new Audio('sound/hack.wav');
  		audio.play();
  // 		$('#loose').offset({
		//     left : 300
		// });
		$('#loose').slideDown(500);
		$('#loose').slideUp(300);
	}

//*******Desk
	function getOneDesk() {
		//...
	}
	
	function setDesk(v) {
		//...
		desk = v;
	}
	
	function loseOneDesk() {
		//...
		getOneSkills();
		getOneLife();
		$("img.desk").eq(desk-1).hide();
		desk--;
		audio = new Audio('sound/gloups.wav');
  		audio.play();
  // 		$('#loose').offset({
		//     left : 300
		// });
		$('#loose').slideDown(500);
		$('#loose').slideUp(300);
	}

//*******Saves
	function getSaves() {
		//...
	}
	
	function setSaves(v) {
		//...
		saves = v;
	}
	
	function loseOneSaves() {
		//...
	}
	
	function startGame() {
		//...
		$("div.section").hide();
		$("#man").hide();
		$("#loose").hide();
		$("button").hide();
		$("div.section").eq(0).fadeIn(1000);
		$("button").fadeIn(1500);
		ecriture('intro');
		setLife(3);
		setSkills(3);
		setDesk(3);
		setSaves(3);
		codeur = 5;
		audio = new Audio('sound/sos.mp3');
  		audio.play();
  		$("#saveText").hide();
  		$(".fin").hide();

	}
	
	function endGame() {
		//...
		$("div.section").hide();
		gotoSection('death');
		ambiance.pause();
		audio = new Audio('sound/death.mp3');
  		audio.play();
		$('body').css('background-image', 'url("img/backDeath.gif")');
		$('body').css('background-size', 'cover');
		$('#death').css('background-color', 'black');
		$('#death').css('padding-top', '20px');
	}
	function restart() {
		//...
		startGame();
		$("img.life").show();
		$("#answer .typed-strings p").remove();
		var p='<p id="question">Oh ! Enfin ! Tu me répond ! Qui es-tu exactement ?</p>';
		$("#answer .typed-strings").append(p);
		$("#reponse .typed-strings p").remove();
		$("#refus .typed-strings p").remove();
		$("img.kit").show();
		$('body').css('background-image', 'none');
		ambiance.pause();
		ambiancerapide.pause();
		location.reload();


	}
	
} );

//***************Horloge*****************************

function clock(){

//Save the times in variables

var today = new Date();

var hours = today.getHours();
var minutes = today.getMinutes();
var seconds = today.getSeconds();


//Set the AM or PM time

if (hours >= 12){
  meridiem = " PM";
}
else {
  meridiem = " AM";
}


//convert hours to 12 hour format and put 0 in front
if (hours>12){
	hours = hours - 12;
}
else if (hours===0){
	hours = 12;	
}

//Put 0 in front of single digit minutes and seconds

if (minutes<10){
	minutes = "0" + minutes;
}
else {
	minutes = minutes;
}

if (seconds<10){
	seconds = "0" + seconds;
}
else {
	seconds = seconds;
}


document.getElementById("clock").innerHTML = (hours + ":" + minutes + ":" + seconds + meridiem);

}


setInterval('clock()', 1000);

//****************************Typed******************

function ecriture(key){

        $("#" + key + " .typed").typed({
            // strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
            stringsElement: $("#" + key + " .typed-strings"),
            typeSpeed: 30,
            backDelay: 500,
            loop: false,
            contentType: 'html', // or text
            // defaults to false for infinite loop
            loopCount: false,
            callback: function(){},
            resetCallback: function() { newTyped(); }
        });

        $(".reset").click(function(){
            $(".typed").typed('reset');
        });

    };

    function newTyped(){ /* A new typed object */ }

    function foo(){ console.log("Callback"); }



	function manMove(){
		$("#man").fadeIn(1000);
		$( "#man" ).animate({
			   marginLeft: "17.8in",
		}, 12000 );
		$( "#man" ).fadeOut(800);
	

	}




